﻿using FluentValidation;
using SolucionIoT.COMMON.Entidades;
using System;
using System.Collections.Generic;
using System.Text;

namespace SolucionIoT.COMMON.Validadores
{
    public class LecturaValidator:GenericValidator<Lectura>
    {
        public LecturaValidator()
        {
            RuleFor(l => l.Temperatura).NotNull().NotEmpty().InclusiveBetween(-50, 150);
            RuleFor(l => l.Humedad).NotNull().NotEmpty().InclusiveBetween(-10, 150);
            RuleFor(l => l.Luminosidad).NotNull().NotEmpty().InclusiveBetween(-10, 150);
            RuleFor(l => l.IdDispositivo).NotNull().NotEmpty();
        }
    }
}
