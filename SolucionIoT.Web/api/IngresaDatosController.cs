﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SolucionIoT.BIZ;
using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Interfaces;

namespace SolucionIoT.Web.api
{
    [Route("api/[controller]")]
    [ApiController]
    public class IngresaDatosController : ControllerBase
    {
        IUsuarioManager usuarioManager;
        IDispositivoManager dispositivoManager;
        ILecturaManager lecturaManager;
        IAccionManager accionManager;
        public IngresaDatosController()
        {
            usuarioManager = FactoryManager.UsuarioManager();
            dispositivoManager = FactoryManager.DispositivoManager();
            lecturaManager = FactoryManager.LecturaManager();
            accionManager = FactoryManager.AccionManager();
        }

        [HttpPut]
        public ActionResult<Accion> Put(string correo, string password, string idDispositivo,string actuador,bool estado)
        {
            Usuario u = usuarioManager.Login(correo, password);
            if (u != null)
            {
                if (dispositivoManager.DispositivoPerteneceAUsuario(idDispositivo, u.Id)!=null)
                {
                    try
                    {
                        return Ok(accionManager.Insertar(new Accion()
                        {
                            Actuador=actuador,
                            Estado=estado,
                            IdDispositivo=idDispositivo
                        }));
                    }
                    catch (Exception)
                    {
                        return BadRequest("No se pudo crear la Accion " + lecturaManager.Error);
                    }
                }
                else
                {
                    return BadRequest("Dispositivo no pertenece al usuario...");
                }
            }
            else
            {
                return BadRequest("Usuario y/o contraseña incorrecta...");
            }
        }

        // POST: api/IngresaDatos
        [HttpPost]
        public ActionResult<Lectura> Post(string correo, string password,string idDispositivo,float temperatura, float humedad, float luminosidad)
        {
            Usuario u = usuarioManager.Login(correo, password);
            if (u != null)
            {
                if(dispositivoManager.DispositivoPerteneceAUsuario(idDispositivo, u.Id)!=null)
                {
                    try
                    {
                        return Ok(lecturaManager.Insertar(new Lectura()
                        {
                            Humedad=humedad,
                            IdDispositivo=idDispositivo,
                            Luminosidad=luminosidad,
                            Temperatura=temperatura
                        }));
                    }
                    catch (Exception)
                    {
                        return BadRequest("No se pudo crear la lectura " + lecturaManager.Error);
                    }
                }
                else
                {
                    return BadRequest("Dispositivo no pertenece al usuario...");
                }
            }
            else
            {
                return BadRequest("Usuario y/o contraseña incorrecta...");
            }
        }

    }
}
