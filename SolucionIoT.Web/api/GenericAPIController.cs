﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SolucionIoT.COMMON.Entidades;
using SolucionIoT.COMMON.Interfaces;
using SolucionIoT.COMMON.Modelos;

namespace SolucionIoT.Web.api
{
    [Route("api/[controller]")]
    [ApiController]
    public abstract class  GenericAPIController<T> : ControllerBase where T:BaseDTO
    {
        IGenericManager<T> manager;
        public GenericAPIController(IGenericManager<T> genericManager)
        {
            manager = genericManager;
        }
        // GET: api/GenericAPI
        [HttpGet]
        public ActionResult<IEnumerable<T>> Get()
        {
            try
            {
                return Ok(manager.ObtenerTodos);
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // GET: api/GenericAPI/5
        [HttpGet("{id}")]
        public ActionResult<T> Get(string id)
        {
            try
            {
                return Ok(manager.BuscarPorId(id));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // POST: api/GenericAPI
        [HttpPost]
        public ActionResult<T> Post([FromBody] T value)
        {
            try
            {
                return Ok(manager.Insertar(value));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // PUT: api/GenericAPI/5
        [HttpPut]
        public ActionResult<T> Put([FromBody] T value)
        {
            try
            {
                return Ok(manager.Actualizar(value));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult<bool> Delete(string id)
        {
            try
            {
                return Ok(manager.Eliminar(id));
            }
            catch (Exception)
            {
                return BadRequest(manager.Error);
            }
        }

        [HttpPost("{id}")]
        public abstract ActionResult<IEnumerable<T>> Consulta([FromBody] ConsultaAPIModel model, string id);

    }
}
