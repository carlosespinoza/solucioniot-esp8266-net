﻿namespace SolucionIoT.Tools
{
    public class MensajeRecibido
    {
        public MensajeRecibido()
        {
        }

        public string Mensaje { get; set; }
        public string Topico { get; set; }
    }
}